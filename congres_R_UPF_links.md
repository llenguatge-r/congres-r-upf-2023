# Alguns enllaços o fitxers desordenats del congrés de R


### *Gols, passades, xuts i molt més: futbol i dades amb R*, Roger Tugas (Nació Digital)

+ Fitxers utilitzats: [TallerCongresR1.R](./TallerCongresR1.R), [TallerCongresR2.R](./TallerCongresR2.R)


### *Automatización de scrapping web con R y GitHub*, Adrian Maqueda (Civio)

+ [Enllaç](https://scrapping-con-r.vercel.app/)


### *"Data Driven" o "Excel Driven", datos abiertos y R un ejemplo real* Carlos Ortega (Quality Excellence)

+ S'ha de buscar el vídeo. Showman molt interessant.

### *Creación de mapas personalizados de España con R y MapSpain* Alguacil García, Sara (MC MUTUAL)

+ [Enllaç](https://ropenspain.github.io/mapSpain/)

### *Fábricas de chunks*	Irene Cruz Gómez (Universidad Autónoma de Barcelona)

+ [Enllaç que necessita compte a posit.cloud](https://posit.cloud/content/7070482)

### *Fundamentos de ciencia de datos con R* Gema Fernández-Avilés Calderón (Universidad de Castilla-La Mancha)

+ [Llibre en obert](https://cdr-book.github.io/)

### *R-shiny application of the evolution of COVID-19 in Catalonia with Bayesian spatio-temporal analysis* Pau Satorra (IGTP)
+ [Enllaç de l'app de Shiny](https://brui.shinyapps.io/covidcat_evo/)

### *Reemplazando la torre de Babel: herramientas para multilingüismo en R* Maëlle Salmon
+ [Enllaç](https://babel-bcn.netlify.app)

### *Com pensar clar: Vuit regles per analitzar qualsevol cosa* Kiko Llaneras (El País)

+ [Enllaç a un article relacionat amb la xerrada; recomanable i potent el directe](https://kiko.llaneras.es/ocho-reglas-para-pensar-claro)
